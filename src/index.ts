import dotenv from 'dotenv';
dotenv.config();

import http from 'http';
import connectToDatabase from './utils/database';
import logger from './utils/logger';
import app from './app';


const server = http.createServer(app);

const normalizePort = (val: string | undefined): number => {
  if (!val) return 5000;
  const port = parseInt(val)

  if (isNaN(port)) {
    return 5000
  } 
  return port;
}

const PORT: number = normalizePort(process.env.PORT);

server.listen(PORT);

server.on('listening', async () => {
  await connectToDatabase();
  logger.info(`Application is listening on port ${PORT}`);
});

server.on('close', () => {
  logger.info('Application server closed');
});

