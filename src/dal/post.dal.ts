import PostModel from '../models/post.model'
import { NotFoundError } from '../middlewares/error';
import { Post, PostInput } from '../interfaces/post.interface';


export interface FindOptions<T> {}

export const create = async (payload: PostInput): Promise<Post> => {
  const post = await PostModel.create(payload)
  return post
}

export const update = async (id: string, payload: Partial<PostInput>): Promise<Post> => {
  const post = await PostModel.findById(id)
  if (!post) {
    throw new NotFoundError(`Post with id: ${id} not found`);
  }
  return await post.update(payload);
}

export const findById = async (id: string): Promise<Post> => {
  const post = await PostModel.findById(id)

  if (!post) { 
    throw new NotFoundError(`Post with id: ${id} not found`);
  }
  return post
}

export const findOne = async (options?: FindOptions<PostInput>): Promise<Post> => {
  const post = await PostModel.findOne({ ...options })

  if (!post) {
    throw new NotFoundError(`Post not found`);
  }
  return post
}

export const deleteById = async (id: string): Promise<boolean> => {
  const deletedPostCount = await PostModel.deleteOne({
    where: { postId: id }
  });

  return !!deletedPostCount;
}

export const findAll = async (options?: FindOptions<PostInput>): Promise<Post[]> => { 
  return PostModel.find({ ...options });
}


