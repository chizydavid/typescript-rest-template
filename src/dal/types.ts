interface ListFilters {
  isDeleted?: boolean
  includeDeleted?: boolean
}

export interface GetAllPostsFilters extends ListFilters {
  userId?: boolean
}

