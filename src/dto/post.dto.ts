import { PostInput } from "../interfaces/post.interface";


export interface CreatePostDTO extends PostInput {}

export interface UpdatePostDTO extends PostInput  {}

// export interface FilterPostsDTO {
//   isDeleted?: boolean
//   includeDeleted?: boolean
// }
