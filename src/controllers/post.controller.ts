import PostService from "../services/post.service"
import { Response, NextFunction, Request } from 'express';
import { CreatePostDTO, UpdatePostDTO } from "../dto/post.dto";
import logger from "../utils/logger";


const createPost = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const post = await PostService.create(req.body as CreatePostDTO);
    res.status(200).json({ 
      data: post
    });
  } catch(error: any) {  
    logger.error(error as string)
    next(error)
  }
}

const updatePost = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { postId } = req.params;
    const post = await PostService.update(postId, req.body as UpdatePostDTO);

    res.status(200).json({ 
      data: post
    });
  } catch(error) {
    logger.error(error as string)
    next(error)
  }
}

/**
 * Fetches all Posts
 *
 */
const getAllPosts = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const atTime = req.query.at as string;
    const posts = await PostService.findAll(atTime);
    res.status(200).json({ 
      data: {
        at: posts[0].createdAt,
        posts
      }
     });
  } catch(error) {
    logger.error(error as string)
    next(error)
  }
}  

/**
 * Fetches Single Post
 *
 */
const getPost = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const atTime = req.query.at as string;
    const { postId } = req.params;
    const post = await PostService.findOne(parseInt(postId), atTime);
    res.status(200).json({ 
      data: {
        at: post.createdAt,
        post,
      }
    });
  } catch(error) {
    logger.error(error as string)
    next(error)
  }
}

const deletePost = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { postId } = req.params;  
    await PostService.deleteById(postId);
    res.status(204).send();
  } catch(error) {
    logger.error(error as string)
    next(error)
  }
}

export default {
  getAllPosts,
  getPost,
  createPost,
  updatePost,
  deletePost
}


