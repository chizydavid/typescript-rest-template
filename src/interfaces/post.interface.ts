import { IPost } from '../models/post.model';

export interface PostInput {
  title: string;
  body?: string;
  userId?: string;
}

export interface Post extends IPost{}

