import * as postDal from '../dal/post.dal'
import { Post, PostInput } from '../interfaces/post.interface';
import { NotFoundError } from '../middlewares/error';


export const create = async (payload: PostInput): Promise<Post> => {
  try {
    return await postDal.create(payload)
  } catch(error) {
    throw error;
  }
}

export const update = async (id: string, payload: Partial<PostInput>): Promise<Post> => {
  try {
    return await postDal.update(id, payload)
  } catch(error) {
    throw error;
  }
} 

export const findById = (id: string): Promise<Post> => {
  const post = postDal.findById(id)
  return post;
}

export const deleteById = (id: string): Promise<boolean> => {
  try {
    return postDal.deleteById(id)
  } catch(error) {
    throw error;
  }
}

const findAll = async (atTime: string): Promise<Post[]> => {
  const data = await postDal.findAll({
    where: { 

    },
  });
  return data;
}

const findOne = async (postId: number, atTime: string) => {
  const data = await postDal.findOne({
    where: {
      postId,
      createdAt: {

      }
    },
    order: [['createdAt', 'DESC']]
  });
  return data;
}

export default {
  create,
  update,
  findById,
  deleteById,  
  findOne,
  findAll
}


