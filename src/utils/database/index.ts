import mongoose from 'mongoose';
import APP_CONFIG from '../../config/app.config';
import logger from '../logger';

const { DB_URL } = APP_CONFIG;


export default async (): Promise<void> => {
  return new Promise((resolve, reject) => {
    mongoose.connect(DB_URL).then(() => {
      console.log(DB_URL)
      logger.info('Database connection established successfully');
      resolve();
    })
    .catch((error: any) => {
      logger.error(`Unable to connect to  the  database, ${error}`)
    })
  });
}
