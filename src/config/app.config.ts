const { 
  NODE_ENV, 
  DB_HOST,
  DB_NAME, 
  DB_USER, 
  DB_PASSWORD, 
  DB_URI,
  DB_URL,
  PORT
} = process.env;


export default {
  ENV: NODE_ENV,
  PORT: PORT!,
  DB_USER: DB_USER!,
  DB_HOST: DB_HOST!,
  DB_NAME: DB_NAME!,
  DB_PASSWORD: DB_PASSWORD!,
  DB_URL: DB_URL!,
  DB_URI
}
