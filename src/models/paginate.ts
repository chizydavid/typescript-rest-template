import { Document, PaginateModel, AggregatePaginateModel } from 'mongoose';


export interface IPaginateModel<T extends Document> 
    extends PaginateModel<T>, AggregatePaginateModel<T> {}
