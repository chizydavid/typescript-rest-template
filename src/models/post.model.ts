import { Schema, model, Document, Types } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';
import aggregatePaginate from 'mongoose-aggregate-paginate-v2';
import { IPaginateModel } from './paginate';


export interface IPost extends Document {
  title: string;
  body?: string;
  userId?: string;
  createdAt: Date;
  updatedAt: Date;
}


const PostSchema: Schema = new Schema(
  {
    title: { type: String },
    body: { type: String },
    userId: { type: Schema.Types.ObjectId }
  },
  {
    timestamps: true
  }
)

PostSchema.plugin(mongoosePaginate)
PostSchema.plugin(aggregatePaginate)

interface PostModel<T extends Document> extends IPaginateModel<T> {};
const PostModel = model<IPost>('Post', PostSchema) as PostModel<IPost>;

export default PostModel;
