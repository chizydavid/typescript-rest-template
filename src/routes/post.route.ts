import express, { Request, Response } from 'express';
import PostController from '../controllers/post.controller';
import { 
  validateISOFormat as validatePostTime
} from '../middlewares/validator';

const router = express.Router();

router.post(
  "/",
  PostController.createPost
)

router.put(
  "/:postId",
  PostController.updatePost
)

router.get(
  "/",
  validatePostTime,
  PostController.getAllPosts
);

router.get(
  "/:postId",
  validatePostTime,
  PostController.getPost
);

router.delete(
  "/:postId",
  PostController.deletePost
)

export default router;

